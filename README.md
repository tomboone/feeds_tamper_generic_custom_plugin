# Feeds Tamper Generic Custom Plugin #

Skeleton code for writing custom Feeds Tamper plugin modules. On deployment, replace 'feeds_tamper_generic_custom_plugin' with your desired module/plugin machine name in all file and function names, and customize the module's .info file.

Contains only enough code to make custom plugins functional within the Drupal/Feeds/Feeds Tamper framework. Included plugin contains only the base code. Actual forms and functions will need to be added to the plugin code.