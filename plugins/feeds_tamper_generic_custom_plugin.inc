<?php
/*
*
 * @file
 * A generic Fields Tamper plugin skeleton.
 */

$plugin = array(
    'form' => 'feeds_tamper_generic_custom_plugin_form',
    // Optional validation callback.
    // 'validate' => 'feeds_tamper_generic_custom_plugin_validate',
    'callback' => 'feeds_tamper_generic_custom_plugin_callback',
    'name' => 'Generic Feeds Tamper Plugin Skeleton',
    'multi' => 'direct',
    'category' => 'Other',
);

function feeds_tamper_generic_custom_plugin_replace_form($importer, $element_key, $settings) {
    $form = array();
    // Define field forms
    return $form;
}

function feeds_tamper_generic_custom_plugin_callback($source, $item_key, $element_key, &$field, $settings) {
    $field = some_cool_function($field);
}
